# Create a Person class that is an abstract class
from abc import ABC, abstractclassmethod

class Person(ABC):
	@abstractclassmethod
	def getFullName(self):
		pass

	@abstractclassmethod
	def addRequest(self):
		pass

	@abstractclassmethod
	def checkRequest(self):
		pass

	@abstractclassmethod
	def addUser(self):
		pass


# Create an Employee class from Person
class Employee(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

	# setters
	def set_firstName(self, firstName):
		self._firstName = firstName

	def set_lastName(self, lastName):
		self._lastName = lastName

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department

	# getters
	def get_firstName(self):
		return self._firstName

	def get_lastName(self):
		return self._lastName

	def get_email(self):
		return self._email

	def get_department(self):
		return self._department

	def getFullName(self):
		return (f"{self._firstName} {self._lastName}")

	def addRequest(self):
		message = f"Request has been added"
		return message

	# abstract methods
	def checkRequest(self):
		message = f"Your request is"
		return message

	def addUser(self):
		message = f"User has been added"
		return message

	def login(self):
		message = f"{self._email} has logged in"
		return message

	def logout(self):
		message = f"{self._email} has logged out"
		return message


# Create an TeamLead class from Person
class TeamLead(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department
		self._memberList = []

	# setters
	def set_firstName(self, firstName):
		self._firstName = firstName

	def set_lastName(self, lastName):
		self._lastName = lastName

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department

	# getters
	def get_firstName(self):
		return self._firstName

	def get_lastName(self):
		return self._lastName

	def get_email(self):
		return self._email

	def get_department(self):
		return self._department

	def getFullName(self):
		return f"{self._firstName} {self._lastName}"

	def addRequest(self):
		message = f"Request has been added"
		return message

	# abstract methods
	def checkRequest(self):
		message = f"Your request is"
		return message

	def addUser(self):
		message = f"User has been added"
		return message

	def login(self):
		message = f"{self._email} has logged in"
		return message

	def logout(self):
		message = f"{self._email} has logged out"
		return message

	def addMember(self, Employee):
		self._memberList.append(Employee)

	def get_members(self):
		return self._memberList


# Create an Admin class from Person
class Admin(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

	# setters
	def set_firstName(self, firstName):
		self._firstName = firstName

	def set_lastName(self, lastName):
		self._lastName = lastName

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department

	# getters
	def get_firstName(self):
		return self._firstName

	def get_lastName(self):
		return self._lastName

	def get_email(self):
		return self._email

	def get_department(self):
		return self._department

	def getFullName(self):
		return f"{self._firstName} {self._lastName}"

	def addRequest(self):
		message = f"Request has been added"
		return message

	# abstract methods
	def checkRequest(self):
		message = f"Your request is"
		return message

	def addUser(self): 
		message = f"User has been added"
		return message

	def login(self):
		message = f"{self._email} has logged in"
		return message

	def logout(self):
		message = f"{self._email} has logged out"
		return message


# Create a request class
class Request():
	def __init__(self, name, requester, dateRequested, status = "Open"):
		self._name = name
		self._requester = requester
		self._dateRequested = dateRequested
		self._status = status

	def updateRequest(self):
		message = f"Request {self._name} is updated"
		return message

	def closeRequest(self):
		message = f"Request {self._name} is closed"
		return message

	def cancelRequest(self):
		message = f"Request {self._name} is cancelled"
		return message

# Test cases
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

# Assertion
assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

# Test cases
teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
	print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"
print(req2.closeRequest())
